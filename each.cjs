function each(elements, cb) {
     if (elements === null || typeof elements !== 'object' || cb === undefined) {
          return elements
     }
     let array = []
     for (let index in elements) {
          array.push(cb(elements[index]))
     }
     return array
}

module.exports = each