function filter(elements, cb) {

    if (elements === null || elements === undefined || cb === undefined) {
        return []
    }
    let array = []
    for (let num of elements) {
        let result = cb(num)
        if (result) {
            array.push(num)
        }
    }
    return array
}

module.exports = filter