function find(elements, cb) {
    if (elements === null || elements === undefined || elements === [] || elements === {}) {
        return undefined
    }
    if (Array.isArray(elements) && cb !== undefined) {
        let result
        for (let num of elements) {
            result = cb(num)
            if (result) {
                return num
                break
            }
        }
        return result = undefined
    }
    return elements[0]
}

module.exports = find