function flatten(array, depth = Infinity) {
    if (array === null || array === undefined || array === []) {
        return []
    }
    const flatArr = [];

    for (let i = 0; i < array.length; i++) {
        const element = array[i];

        if (Array.isArray(element) && depth > 0) {
            flatArr.push(...flatten(element, depth - 1));
        } else {
            flatArr.push(element);
        }
    }

    return flatArr;
}


module.exports = flatten