function map(elements, cb) {
    let array = []
    if (elements === undefined || elements === null || typeof elements === 'string') {
        return []

    } else if (cb !== undefined) {
        for (let index in elements) {
            array.push(cb(elements[index]))
        }
        return array
    }
    else if (cb === undefined){
        return elements
    }
}

module.exports = map