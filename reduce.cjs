function reduce(elements, cb, startingValue) {
    if (cb === undefined) {
        return "Type Error"
    }
    let array = 0
    if (startingValue !== undefined) {
        array = array + (cb(startingValue))
    }
    for (let i in elements) {
        array = array + (cb(elements[i]))
    }
    return array
}

module.exports = reduce