const find = require('../find.cjs')

const items = [1,3,4,5,5,8];

let findElement = find(items,function cb(num){
    let result = (num%2 == 0)
    return result
})

console.log(findElement)

console.log(find('abc'))
console.log(find(undefined));
console.log(find([1, 2, 3]));
console.log(find([]));